// console.log('Hello World');

// [PART 1]

    const getCube = (num) => {
        console.log(`The cube of ${num} is ${num ** 3}`);
    };

    getCube(2);

// [PART 2]

    const address = ['258', 'Washington Ave NW', 'California', '90011'];
    const [streetNum, streetName, state, zipCode] = address;

    console.log(`I live at ${streetNum} ${streetName}, ${state} ${zipCode}`);

// [PART 3]

    const animal = {
        name: 'Lolong',
        type: 'saltwater crocodile',
        weight: 1075,
        lengthFt: 20,
        lengthIn: 3 
    };

    const {name, type, weight, lengthFt, lengthIn} = animal;


    console.log(`${name} is a ${type}. He weighted at ${weight} kgs with a measurement of ${lengthFt} ft ${lengthIn} in.`);

// [PART 4]

    const numbers = [1, 2, 3, 4, 5];

    numbers.forEach((num => 
        console.log(num)
    ));

// [PART 5]

    const reduceNumber = numbers.reduce((x, y) => x + y);

    console.log(reduceNumber);

// [PART 6]

    class Dog {
        constructor(name, age, breed) {
            this.name = name;
            this.age = age;
            this.breed = breed;
        }
    };

    const myDog = new Dog('Frankie', 5, 'Miniature Dachshund');

    console.log(myDog);
